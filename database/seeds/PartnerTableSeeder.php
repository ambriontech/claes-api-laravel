<?php

use Illuminate\Database\Seeder;
use App\Partner as Partner;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Partner::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 10; $i++) {

            Partner::create([
                'date' => $faker->date(),
                'company' => $faker->company(),
                'contact' => $faker->name(),
                'function' => ucwords($faker->sentence()),
                'phone' => $faker->e164PhoneNumber(),
                'mobile' => $faker->e164PhoneNumber(),
                'email' => 'info@'.$faker->word().'.com',
                'info' => $faker->paragraph()
            ]);
        }
    }
}