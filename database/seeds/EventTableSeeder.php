<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Event::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 20; $i++) {
            Event::create([
                'date' => $faker->date(),
                'name' => ucwords($faker->sentence()),
                'location' => $faker->streetAddress().', '.$faker->city(),
                'time_from' => $faker->dateTime(),
                'time_to' => $faker->dateTime(),
                'max_persons' => $faker->randomNumber(2),
                'price' => $faker->numberBetween(100, 1000),
                'info' => $faker->paragraph(),
                'image' => $faker->image()
            ]);
        }
    }
}