<?php

use Illuminate\Database\Seeder;
use App\EconomyArticle as EconomyArticle;

class EconomyArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        EconomyArticle::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 30; $i++) {

            $discount = $faker->randomNumber(2);
            $price = $faker->randomNumber(4);
            $quantity = $faker->randomNumber(1);

            $total = ($price * $quantity) * (1 - ($discount / 100));
            $vat_level = 25;
            $vat_total = $total * ($vat_level / 100);
            $total_incl_vat = $total + $vat_total;

            EconomyArticle::create([
                'order_id' => $faker->randomNumber(1),
                'quote_id' => $faker->randomNumber(1),
                'booking_id' => $faker->randomNumber(1),
                'article_group' => $faker->randomNumber(1),
                'name' => ucwords($faker->sentence()),
                'package' => ucwords($faker->sentence()),
                'quantity' => $quantity,
                'article_id' => $faker->randomNumber(1),
                'date_fr' => $faker->dateTime(),
                'date_to' => $faker->dateTime(),
                'serv_time' => $faker->time($format = 'H:i'), // 20:49
                'discount' => $discount,
                'unit' => $faker->randomNumber(1),
                'price' => $price,
                'vat_level' => $vat_level,
                'total' => $total,
                'vat_total' => $vat_total,
                'total_incl_vat' => $total_incl_vat
            ]);
        }
    }
}