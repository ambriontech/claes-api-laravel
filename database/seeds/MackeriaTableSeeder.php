<?php

use Illuminate\Database\Seeder;
use App\Mackeria as Mackeria;

class MackeriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Mackeria::truncate();
        
        $faker = \Faker\Factory::create();

        $numCustomers = 3;

        $numMackerias = 10;

        // And now, let's create the items in our database:
        for ($i = 0; $i < $numMackerias; $i++) {

            $total = $faker->randomNumber(4);
            $vat_total = $total * 0.25;
            $total_incl_vat = $total + $vat_total;

            Mackeria::create([
                'date' => $faker->date(),
                'customer_id' => ceil((($i+1) / $numMackerias) * $numCustomers),
                'contact_id' => $faker->randomNumber(1),
                'delivery_date' => $faker->date(),
                'delivered' => $faker->randomElement([0, 1]),
                'info' => $faker->paragraph(),
                'total' => $total,
                'vat_total' => $vat_total,
                'total_incl_vat' => $total_incl_vat
            ]);
        }
    }
}