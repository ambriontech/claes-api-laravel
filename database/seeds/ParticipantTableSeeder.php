<?php

use Illuminate\Database\Seeder;
use App\Participant as Participant;

class ParticipantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Participant::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 50; $i++) {

            Participant::create([
                'date' => $faker->date(),
                'name' => $faker->name(),
                'company' => $faker->company(),
                'mobile' => $faker->e164PhoneNumber(),
                'email' => $faker->email(),
                'event_id' => $faker->randomNumber(1)
            ]);
        }
    }
}