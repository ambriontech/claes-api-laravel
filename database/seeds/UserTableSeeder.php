<?php

use Illuminate\Database\Seeder;
use App\User as User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        User::truncate();
        
        $faker = \Faker\Factory::create();

        $numCustomers = 3;

        $numUsers = 20;

        // And now, let's create the items in our database:
        for ($i = 0; $i < $numUsers; $i++) {
            User::create([
                'name' => $faker->name(),
                'email' => $faker->email(),
                'password' => bcrypt('1234'),
                'company_id' => ceil((($i+1) / $numUsers) * $numCustomers),
                'phone' => $faker->e164PhoneNumber(),
                'mobile' => $faker->e164PhoneNumber(),
                'main_contact' => $i % ($numUsers / $numCustomers) ? 0 : 1,
                'info' => $faker->paragraph(),
                'category' => $faker->randomElement([0, 1, 2]),
                'permission' => 0,
            ]);
        }
    }
}
