<?php

use Illuminate\Database\Seeder;
use App\PackageArticle as PackageArticle;

class PackageArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        PackageArticle::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 20; $i++) {

            PackageArticle::create([
                'article_id' => $faker->randomNumber(1),
                'package_id' => $faker->randomNumber(1)
            ]);
        }
    }
}
