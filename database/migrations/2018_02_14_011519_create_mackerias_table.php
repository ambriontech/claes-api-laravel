<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMackeriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mackerias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 10)->nullable();
            $table->integer('customer_id')->default(0);
            $table->integer('contact_id')->default(0);
            $table->string('delivery_date', 10)->nullable();
            $table->tinyInteger('delivered')->default(0);
            $table->text('info')->nullable();
            $table->integer('total')->default(0);
            $table->integer('vat_total')->default(0);
            $table->integer('total_incl_vat')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mackerias');
    }
}
