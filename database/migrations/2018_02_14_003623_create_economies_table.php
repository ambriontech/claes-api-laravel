<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEconomiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('economies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 10)->nullable();
            $table->string('serial_quote', 100)->nullable();
            $table->string('serial_version_quote', 100)->nullable();
            $table->string('serial_booking', 100)->nullable();
            $table->string('serial_order', 100)->nullable();
            $table->integer('customer_id')->default(0);
            $table->string('name', 100)->nullable();
            $table->text('payment_conditions')->nullable();
            $table->string('valid_to', 10)->nullable();
            $table->tinyInteger('category')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->text('delivery_terms')->nullable();
            $table->text('room_info')->nullable();
            $table->text('av_info')->nullable();
            $table->text('price_info')->nullable();
            $table->text('meal_info')->nullable();
            $table->integer('number_of_persons')->default(0);
            $table->string('number_of_persons_text', 100)->nullable();
            $table->text('info')->nullable();
            $table->integer('total')->default(0);
            $table->integer('vat_total')->default(0);
            $table->integer('total_incl_vat')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('economies');
    }
}

