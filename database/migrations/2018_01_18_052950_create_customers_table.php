<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 10)->nullable();
            $table->string('workplace', 100)->nullable();
            $table->integer('room_category')->default(-1);
            $table->integer('member_category')->default(-1);
            $table->string('name', 100)->nullable();
            $table->string('registration_number', 50)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('zip_code', 5)->nullable();
            $table->string('town', 30)->nullable();
            $table->string('visiting_address', 100)->nullable();
            $table->string('visiting_zip_code', 5)->nullable();
            $table->string('visiting_town', 30)->nullable();
            $table->text('info')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
