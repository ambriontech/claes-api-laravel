<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 10)->nullable();
            $table->string('name', 200)->nullable();
            $table->integer('unit')->default(0);
            $table->integer('price')->default(0);
            $table->integer('vat_level')->default(0);
            $table->integer('article_group')->default(0);
            $table->integer('result_unit')->default(0);
            $table->text('info')->nullable();
            $table->integer('layout1')->default(0);
            $table->integer('layout2')->default(0);
            $table->integer('layout3')->default(0);
            $table->tinyInteger('bookable')->default(0);
            $table->string('image', 200)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
