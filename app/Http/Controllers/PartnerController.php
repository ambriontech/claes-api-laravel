<?php

namespace App\Http\Controllers;

use App\Partner as Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    protected static $model = "App\Partner";
}
