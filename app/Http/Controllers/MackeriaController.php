<?php

namespace App\Http\Controllers;

use App\Mackeria as Mackeria;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MackeriaController extends Controller
{
    protected static $model = "App\Mackeria";

    protected static $searchFields = ['customer_id', 'contact_id'];

    protected static $joins = [
        ['leftJoin', ['customers', 'customers.id', '=', 'mackerias.customer_id']],
        ['leftJoin', ['users', 'users.id', '=', 'mackerias.contact_id']]
    ];

    protected static $select = [
        'mackerias.*',
        'customers.name as customer_name',
        'users.name as contact_name',
        'users.email as contact_email',
        'users.mobile as contact_moble'
    ];
    public function index(Request $request){
        $allgetValues=$request->all();
        $query=Mackeria::leftjoin('customers', 'customers.id', '=', 'mackerias.customer_id')
        ->leftjoin('users', 'users.id', '=', 'mackerias.contact_id')
        ->select('mackerias.*','customers.name as customer_name','users.name as contact_name','users.email as contact_email','users.mobile as contact_moble');
        if(isset($allgetValues['startdate']) && isset($allgetValues['endDate']) && !empty($allgetValues['endDate']) && isset($allgetValues['all'])) {
            return $query->whereBetween('mackerias.delivery_date',[$allgetValues['startdate'],$allgetValues['endDate']])->get();

        }
        else if(isset($allgetValues['startdate']) && isset($allgetValues['all'])) {
            return $query->where('mackerias.delivery_date', '>=', $allgetValues['startdate'])->get();
        }else if(isset($allgetValues['all'])) {
           return $query->get(); 
        }else{
            return $query->where('mackerias.created_at', '>=', Carbon::today())->get();
        }
        
    }

    public function show(int $id){
        return $mackeria=Mackeria::with(['mackeriaArticles'])
        ->leftjoin('customers', 'customers.id', '=', 'mackerias.customer_id')
        ->leftjoin('users', 'users.id', '=', 'mackerias.customer_id')
        ->select('mackerias.*','customers.name as customer_name','users.name as contact_name','users.email as contact_email','users.mobile as contact_moble')
        ->find($id);
    }
}
