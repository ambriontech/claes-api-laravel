<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article as Article;
use App\Event as Event;
use App\packageArticle as packageArticle;
use Illuminate\Support\Facades\Storage;
use DB;

class ArticleController extends Controller
{
    protected static $model = "App\Article";

    protected $storageImagePath;
    protected $orderBy = 'created_at';
    protected $orderDirection = 'desc';

    public function __construct()
    {
        $this->storageImagePath = url('/').'/storage/app/';
    }

    public function index(Request $request){
    	return Article::with(['group'])
        ->orderBy('created_at', 'desc')
        ->get();
    }

    public function store(Request $request){
    	if($request->isMethod('post')){
    		$allPostedValues=$request->all();
    		$allPostedValues['image']=$this->uploadImage($request);
    		$lastInsertedId=DB::table('articles')->insertGetId($allPostedValues);
    		$article=new Article;
    		return $article->find($lastInsertedId);
    		
    	}
    	
    }

    public function updateImage($id,$model,Request $request){
    	if($request->isMethod('post')){
    		$imagePath=$this->uploadImage($request);
    		switch ($model) {
    			case 'article':
    				$table=new Article;
    				$query=DB::table('articles');
    				break;
    			case 'event':
    				$table=new Event;
    				$query=DB::table('events');
    				break;
    		}
    		//echo $imagePath;die;
            $query->where('id', $id)
            ->update(['image' => $imagePath]);
    		return $table->find($id);
    		
    	}
    	
    }

    public function uploadImage($request){
    	if ($request->hasFile('image')) {
    		$file = $request->image;
    		$path = $request->image->path();
			$extension = $request->image->extension();
		    $pathUploadImage=$request->image->store('images');
		    return $this->storageImagePath.$pathUploadImage;
    	}
    }

    public function filterBy(Request $request){
    	$allPostedValues=$request->all();
    	if($allPostedValues['value']!=''){
    		$field=$allPostedValues['field'];
    		$value=$allPostedValues['value'];
    		$value=explode(",",$value);
    		return $users = Article::whereIn($field, $value)->with(['group'])->get();
    	}else{
    		return $articles=Article::with(['group'])->with(['group'])->get();	
    	}
    	
    }

    private function packageExist($aid,$pid){
        return packageArticle::where(array('article_id'=>$aid,'package_id'=>$pid))->count();
    }

    public function packageArticle($articleId){
        $packageArticleIds = packageArticle::where('article_id',$articleId)->pluck('package_id');
        return $articles= Article::whereNotIn('id', $packageArticleIds)
        ->where('articles.article_group',5)
        ->get();
    }

}
