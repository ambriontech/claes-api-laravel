<?php

namespace App\Http\Controllers;

use App\Participant as Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    protected static $model = "App\Participant";

    protected static $searchFields = ['event_id'];

    protected static $joins = [
        ['leftJoin', ['events', 'events.id', '=', 'participants.event_id']]
    ];

    protected static $select = [
        'participants.*',
        'events.name as event_name'
    ];
}
