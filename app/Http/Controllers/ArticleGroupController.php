<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArticleGroup as ArticleGroup;

class ArticleGroupController extends Controller
{
    protected static $model = "App\ArticleGroup";
}
