<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Economy extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 
        'serial_quote', 
        'serial_version_quote', 
        'serial_booking', 
        'serial_order', 
        'customer_id',
        'name', 
        'payment_conditions', 
        'valid_to', 
        'category', 
        'type', 
        'status', 
        'delivery_terms', 
        'room_info', 
        'av_info', 
        'price_info', 
        'meal_info', 
        'number_of_persons', 
        'number_of_persons_text', 
        'info', 
        'total', 
        'vat_total', 
        'total_incl_vat'
    ];
}
