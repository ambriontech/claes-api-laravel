<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workplace', 
        'room_category', 
        'member_category', 
        'name',
        'registration_number', 
        'phone', 
        'email', 
        'address', 
        'zip_code', 
        'town', 
        'visiting_address', 
        'visiting_zip_code', 
        'visiting_town', 
        'info'
    ];
}
