<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MackeriaArticle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mackeria_id',
        'article_id',
        'date', 
        'quantity',
        'name', 
        'price', 
        'total', 
        'vat_total',
        'total_incl_vat'
    ];

}
