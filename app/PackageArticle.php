<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageArticle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 
        'package_id'
    ];
}
