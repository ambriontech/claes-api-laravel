<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 
        'company',
        'contact', 
        'function', 
        'phone', 
        'mobile', 
        'email', 
        'info'
    ];
}
