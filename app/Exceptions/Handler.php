<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException as MethodNotAllowedHttpException;
use Illuminate\Auth\AuthenticationException as AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\QueryException as QueryException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof MethodNotAllowedHttpException)
        {
            return response()->json(['error' => 'Method not allowed'], 404);
        }
        else if ($e instanceof QueryException)
        {
            return response()->json(['error' => $e->getMessage()], 404);
        }
        else if ($e instanceof AuthException)
        {
            $msg = $e->getMessage();
    
            if($e->getCode() === AuthException::EXCEPTION_AUTH_JSON)
            {
                $msg = json_decode($msg);
            }
    
            return response()->json(['error' => $msg], 401);
        }
        
        return parent::render($request, $e);
    }


    protected function unauthenticated($request, AuthenticationException $e)
    {
        return response()->json(['error' => $e->getMessage()], 401);
    }

}
