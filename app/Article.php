<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 
        'name', 
        'unit', 
        'price', 
        'vat_level', 
        'article_group', 
        'result_unit', 
        'info', 
        'layout1', 
        'layout2', 
        'layout3', 
        'bookable', 
        'image'
    ];

    public function group(){
        return $this->belongsTo('App\ArticleGroup','article_group');
    }

}
